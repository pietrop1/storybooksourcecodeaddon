import React from 'react';

import { Button } from './Button';

export default {
  title: 'With No Code',
  component: Button,
};

const Template = (args) => <Button {...args} />;

export const WithNoCode = Template.bind({});
WithNoCode.args = {
  primary: true,
  label: 'Button',
};
